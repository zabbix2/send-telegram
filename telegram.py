#!/usr/bin/python3

import os, sys, requests
import urllib.parse

bot_token = os.environ.get['TELEGRAM_BOT_TOKEN']
chat = sys.argv[1]
message =  sys.argv[2]

link_main = 'https://api.telegram.org/bot' + bot_token + '/sendMessage'
link_vars = { 'chat_id': chat, 'parse_mode': 'Markdown', 'text': message }
link = link_main + '?' + urllib.parse.urlencode(link_vars)
response = requests.get(link)
